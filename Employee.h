#ifndef EMPLOYEE_H
    #define EMPLOYEE_H
    #include "Employee.h"
    
    class Employee {
        public: 
            virtual ~Employee() {};

            Employee(std::string employeeType, std::string firstName, std::string lastName, int salary) {
                mEmployeeType = employeeType;
                mFirstName = firstName;
                mLastName = lastName;
                mSalary = salary;
            };

            virtual void getInfo() { std::cout << "default"; };

            virtual std::string getEmployeeType() { return "default"; };
            virtual std::string getFirstName() { return "default"; };
            virtual std::string getLastName() { return "default"; };
            virtual int getSalary() { return 0; };
            
            virtual bool getKnowsCpp() { return true; };
            virtual int getYearsOfExperience() { return 0; };
            virtual std::string getEngineerType() { return "default"; };
            
            virtual std::string getPhdMadeAt() { return "default"; };
            virtual std::string getPhdTopic() { return "default"; };

            virtual int getMeetingsPerWeek() { return 0; };
            virtual int getHolidaysPerYear() { return 0; };

        protected:
            std::string mEmployeeType;
            std::string mFirstName;
            std::string mLastName;
            int mSalary;
    };
#endif
